<section class="parallax-apecsa bg-img-2 introduce-wrapper contact-us">
        <div class="bg-overlay-full"></div>
        <div class="container">
          <div class="col-md-12">
            <div class="box-title-desc">
              <div class="title color-white">
                <span>@lang('sentences.contact_us')</span>
              </div>
              <a href="{{route('contactus')}}" class="btn btn-apecsa-red btn-lg">
                <i class="fa fa-envelope"></i>
                @lang('sentences.contact_us')
              </a>
            </div>
          </div>
        </div>
      </section>
<footer class="footer-wrapper diagonal-shadow">
        <div class="container">
          <div class="col-md-12">
            <div class="box-footer">
              <div class="image">
                <img src="{{asset('assets/images/logo/logo2.png')}}" alt="">
              </div>
              <div class="corpyright">
                <span>&copy; Copyright 2020 PT. Baga Putra Mandiri</span><br>
                <span>@lang('sentences.our_logo')</span>
              </div> 
            </div>
          </div>
        </div>
      </footer>