<!DOCTYPE html>
<html>
  @include('head')
  <body>
    <div class="apecsaos-wrapper">
      <!-- START SCOPE HEADER -->
      <!-- <header class="header-wrapper">
        <div class="container">
          <div class="box-header">
            <div class="introduce">
              <span>
                Selamat datang di PT. Baga Putra Mandiri
              </span>
            </div>
            <div class="content">
              <ul>
                <li>
                  <div class="icon">
                    <i class="fa fa-envelope"></i>
                  </div>
                  <span>admin@apecsa-indonesia.com</span>
                </li>
                <li>
                  <div class="icon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <span>(021) 234 454</span>
                </li>
                <li>
                  <div class="icon">
                    <i class="fa fa-map-marker"></i>
                  </div>
                  <span>Cilandak, Jakarta Selatan</span>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </header> -->
      <!-- END SCOPE HEADER -->

      <!-- START SCOPE NAVBAR BOOTSTRAP -->
      @include('menu')
      <!-- END SCOPE NAVBAR BOOTSTRAP -->
      
      <!-- START SCOPE SUB HEADER -->
      <section class="sub-header parallax-apecsa bg-service">
        <div class="bg-overlay-full"></div>
        <div class="container">
          <div class="col-md-12">
            <div class="title">
              @lang('sentences.about_us')
            </div>
          </div>
        </div>
      </section>
      <!-- END SCOPE SUB HEADER -->
      
      <!-- START SCOPE INFORMATION DETAIL ABOUTUS -->
      <section class="content-video bg-white">
        <div class="container">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-6">
                <div class="video-wrapper">
                  <iframe width="100%" height="435" src="https://www.youtube.com/embed/rz_GqnsLPTM" frameborder="0" allowfullscreen></iframe>
                </div>
              </div>
              <div class="col-md-6">
                <div class="content-wrapper">
                  <div class="title">
                    @lang('sentences._about_us'), PT. Baga Putra Mandiri
                  </div>
                  <div class="desc">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#about-visi" aria-expanded="true" aria-controls="about-visi">
                              @lang('sentences.our_vision')
                            </a>
                          </h4>
                        </div>
                        <div id="about-visi" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
						  @lang('sentences._our_vision')
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                          <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#misi" aria-expanded="false" aria-controls="misi">
                              @lang('sentences.our_mission')
                            </a>
                          </h4>
                        </div>
                        <div id="misi" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                          <div class="panel-body">
							@lang('sentences._our_mission')
						  </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                          <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#target" aria-expanded="false" aria-controls="target">
                              @lang('sentences.final_target')
                            </a>
                          </h4>
                        </div>
                        <div id="target" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                          <div class="panel-body">
                            @lang('sentences._final_target')                         
						  </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="text-center">
                  <a href="#" class="btn btn-default btn-lg">
                    <i class="fa fa-download"></i>
                    Company Profile Download
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- END SCOPE INFORMATION DETAIL ABOUTUS -->

      <!-- START SCOPE COUNTING DATA -->
      <div class="counting-data">
        <div class="list-data">
          <ul>
            <li>
              <div class="title">
                Full time resource
              </div>
              <div class="number">
                <span class="incrementalNumber" set-time="3000" data-value="350"></span>
              </div>
            </li>
            <li>
              <div class="title">
                Projects
              </div>
              <div class="number">
                <span class="incrementalNumber" set-time="3000" data-value="150"></span>
              </div>
            </li>
            <li>
              <div class="title">
                Client
              </div>
              <div class="number">
                <span class="incrementalNumber" set-time="3000" data-value="180"></span>
              </div>
            </li>
          </ul>
        </div>
      </div>
      <!-- END SCOPE COUNTING DATA -->
      
      <!-- START SCOPE OUR TEAM -->
      <section class="our-team bg-white">
        <div class="container">
          <div class="col-md-12">
            <div class="box-title-desc">
              <div class="title color-black bdr-btm-gray">
                <span class="bg-white">@lang('sentences.meet_our_team')</span>
              </div>
              <div class="desc color-black">
                @lang('sentences.we_are_team')
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="list-team">
                <div class="col-md-3">
                  <div class="box-user">
                    <div class="image">
                      <img src="assets/images/client/user2.png" alt="Fathan">
                      <div class="overlay">
                        <div class="box-info">
                          <div class="name">Dimas Setyo Utomo</div>
                          <div class="position">CEO</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="box-user">
                    <div class="image">
                      <img src="assets/images/client/user3.png" alt="Fathan">
                      <div class="overlay">
                        <div class="box-info">
                          <div class="name">Rizqi Rahmandaputra</div>
                          <div class="position">CMO</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="box-user">
                    <div class="image">
                      <img src="assets/images/client/user2.png" alt="Fathan">
                      <div class="overlay">
                        <div class="box-info">
                          <div class="name">Yuri Ardila</div>
                          <div class="position">CTO</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="box-user">
                    <div class="image">
                      <img src="assets/images/client/user2.png" alt="Fathan">
                      <div class="overlay">
                        <div class="box-info">
                          <div class="name">Farizan Firdaus</div>
                          <div class="position">CFO</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="box-user">
                    <div class="image">
                      <img src="assets/images/client/user3.png" alt="Fathan">
                      <div class="overlay">
                        <div class="box-info">
                          <div class="name">Andriat Ratyanto</div>
                          <div class="position">Technical Sales</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="box-user">
                    <div class="image">
                      <img src="assets/images/client/user2.png" alt="Fathan">
                      <div class="overlay">
                        <div class="box-info">
                          <div class="name">Uray Harin Hebert</div>
                          <div class="position">Bussines Development</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="box-user">
                    <div class="image">
                      <img src="assets/images/client/user3.png" alt="Fathan">
                      <div class="overlay">
                        <div class="box-info">
                          <div class="name">Christian Tobing</div>
                          <div class="position">Presales Executive</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="box-user">
                    <div class="image">
                      <img src="assets/images/client/user2.png" alt="Fathan">
                      <div class="overlay">
                        <div class="box-info">
                          <div class="name">Anggi Pradityo</div>
                          <div class="position">Human Resource Manager</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="box-user">
                    <div class="image">
                      <img src="assets/images/client/user3.png" alt="Fathan">
                      <div class="overlay">
                        <div class="box-info">
                          <div class="name">Benendycta Meylina</div>
                          <div class="position">Staff</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="box-user">
                    <div class="image">
                      <img src="assets/images/client/user2.png" alt="Fathan">
                      <div class="overlay">
                        <div class="box-info">
                          <div class="name">M. Yusuf Ibrahim</div>
                          <div class="position">Accounting Finance</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="box-user">
                    <div class="image">
                      <img src="assets/images/client/user3.png" alt="Fathan">
                      <div class="overlay">
                        <div class="box-info">
                          <div class="name">Teguh Hartono</div>
                          <div class="position">Quality Assurance</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="box-user">
                    <div class="image">
                      <img src="assets/images/client/user2.png" alt="Fathan">
                      <div class="overlay">
                        <div class="box-info">
                          <div class="name">M. Sulton Hassanudin</div>
                          <div class="position">Senior Developer</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="box-user">
                    <div class="image">
                      <img src="assets/images/client/user3.png" alt="Fathan">
                      <div class="overlay">
                        <div class="box-info">
                          <div class="name">Febryan Setiawan</div>
                          <div class="position">Software Engineering</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="box-user">
                    <div class="image">
                      <img src="assets/images/client/user2.png" alt="Fathan">
                      <div class="overlay">
                        <div class="box-info">
                          <div class="name">Maulana Musyadad</div>
                          <div class="position">Software Engineering</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="box-user">
                    <div class="image">
                      <img src="assets/images/client/user3.png" alt="Fathan">
                      <div class="overlay">
                        <div class="box-info">
                          <div class="name">Fathan Rohman</div>
                          <div class="position">Frontend Developer</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="box-user">
                    <div class="image">
                      <img src="assets/images/client/no-client.gif" alt="Fathan">
                      <div class="overlay">
                        <div class="box-info">
                          <div class="position">We are Hiring!</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- END SCOPE OUR TEAM -->

      <!-- START SCOPE CLIENT & PARTNER  -->
      <!--  
        1. BPKP
        2. PT. Buana Pacific Intenational
        3. Casante International Hotel Management
        4. Niscom Data Solution
        5. Super Soft
        6. Alita
        7. Soiitz
        8. Elitry
        9. Miracle Details
        10. Skillagogo PTE LTD
        11. pemerintah kabupaten tangerang
        12. inotech
        13. BPDP
      -->
      <!-- END SCOPE CLIENT  -->

      <!-- START SCOPE CAREER INTRODUCE -->
      <!-- END SCOPE CAREER INTRODUCE -->

      <!-- START SCOPE FOOTER -->
      @include('footer')
      <!-- END SCOPE FOOTER -->
      <a href="" id="back-to-top" data-toggle="tooltip" data-placement="top" title="Back to top">
        <i class="fa fa-chevron-up"></i>
      </a>
    </div>
  </body>

  <!-- SCOPE JAVASCRIPT -->
  @include('footer_assets')
</html>
